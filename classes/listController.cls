public with sharing class listController {

    //Method to get account list of all accounts or account with specific type, from database
    @AuraEnabled 
    public static List<Account> getAccListApex(String valueType) {
        System.debug('valueType = ' + valueType);

        String accountQuery = '' +
                ' SELECT Id, Name, Image__c, Owner.name, Budget__c, Number_of_Employees__c, Type, Description, Industry ' +
                ' FROM Account ';
        if (String.isNotBlank(valueType)) {
            accountQuery += ' WHERE Type = :valueType ';
        }

        return Database.query(accountQuery);
    }

    //Method to get specific account with specific Id from database
    @AuraEnabled
    public static Account getSingleAcc(String accid){
        try {
            System.debug('accid = ' + accid);
            return [SELECT Id, Name, Type, Industry, Budget__c, Description FROM Account WHERE Id = :accid LIMIT 1];


        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
}