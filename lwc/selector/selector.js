import {LightningElement, api, track} from 'lwc';
import getSingleAcc from '@salesforce/apex/listController.getSingleAcc';

export default class Selector extends LightningElement {
    accId = '';
    accountProperties = {};

    //Getting account id from list.js

    handleAccountSelected(evt) {
        console.log('record has Reached selector.js');
        this.accId = evt.detail;
        console.log('accId = ' + this.accId);
        
        //Calling apex controller to get specific account with specific id

        getSingleAcc({'accid': this.accId})
        .then (result =>{
            this.accountProperties = JSON.parse(JSON.stringify(result));
            console.log('result', JSON.parse(JSON.stringify(result)));
          })
          .catch(error => {console.log('error', error);
        });

    }
}