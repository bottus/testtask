import { LightningElement, wire, api, track } from 'lwc';
import getAccListApex from '@salesforce/apex/listController.getAccListApex';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import ACCOUNT_OBJECT from '@salesforce/schema/Account';
import Type_FIELD from '@salesforce/schema/Account.Type';


export default class List extends LightningElement {

   valueType = '';
   accounts=[];

   //Wires for picklist

    @wire(getObjectInfo, { objectApiName: ACCOUNT_OBJECT })
    objectInfo;

    @wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: Type_FIELD})
    TypePicklistValues;

    //Callback getting list

    connectedCallback() {
        this.fetchAccounts(this.valueType);
    }

    fetchAccounts(valueType) {

        //Getting list of accounts with or without specific Type
        
        getAccListApex(
            { 'valueType': this.valueType }
        )
        .then(result => {
            this.accounts = result;
            console.log('result', JSON.parse(JSON.stringify(result)));
        })
        .catch(error => {
            console.log('error', error);
        });
    }
    
    //Getting picklist value to which picklist has been changed

    handleChange(event) {
        this.valueType = event.detail.value;
        console.log('valuetype', event.detail.value);
        this.fetchAccounts(this.valueType);
    }

    //Getting account Id and passing it to Selector.js

    handleTileClick(evt) {
        
        console.log('record has Reached list.js');
        
        const event = new CustomEvent('accountselected', {
            detail: evt.detail    
        });
        
        this.dispatchEvent(event);
    }
}