import { LightningElement, api, track } from 'lwc';

export default class Tile extends LightningElement {
    @api account;

    //Getting id of this account, and passing it to list.js

    tileClick() {

        const event = new CustomEvent('tileclick', {
        
        detail: this.account.Id
    });
   
        this.dispatchEvent(event);
    }

}